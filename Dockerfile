FROM node
RUN node -v

WORKDIR /backend

COPY ./package.json .

RUN yarn -v
RUN yarn install

COPY ./ ./
ENV NODE_ENV production

EXPOSE 4000
CMD node ./starter.js